package message;

import common.ITextMsg;

/**
 * Implementation of ITextMsg
 * @author qt4
 *
 */
public class TextMsg implements ITextMsg{

	/**
	 * Serial number
	 */
	private static final long serialVersionUID = 371564488071183840L;
	/**
	 * Message text
	 */
	private String text;
	
	/**
	 * Constructor
	 * @param text Message text
	 */
	public TextMsg(String text){
		this.text=text;
	}
	/* (non-Javadoc)
	 * @see common.ITextMsg#getText()
	 */
	@Override
	public String getText() {
		return text;
	}

}

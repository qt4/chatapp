package message;

import common.IRequestCmd;
/**
 * Implementation of IRequestCmd.
 * @author qt4
 *
 */
public class RequestCmd implements IRequestCmd {
	/**
	 * Serial number
	 */
	private static final long serialVersionUID = 8385196379129332406L;
	/**
	 * Class type as ID
	 */
	private Class<?> classtype;
	
	/**
	 * Constructor
	 * @param classtype As ID
	 */
	public RequestCmd(Class<?> classtype){
		this.classtype = classtype;
	}
	
	/* (non-Javadoc)
	 * @see common.IRequestCmd#getID()
	 */
	@Override
	public Class<?> getID() {
		return classtype;
	}

}

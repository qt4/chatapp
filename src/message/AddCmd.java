package message;

import provided.datapacket.ADataPacket;
import provided.datapacket.ADataPacketAlgoCmd;
import common.IAddCmd;

/**
 * Implementation of IAddCmd
 * @author qt4
 *
 */
public class AddCmd implements IAddCmd{
	/**
	 * Serial number
	 */
	private static final long serialVersionUID = -108139619476388825L;
	/**
	 * Class type as ID
	 */
	private Class<?> classtype;
	/**
	 * Command to run
	 */
	private ADataPacketAlgoCmd<ADataPacket, ?, ?> cmd;
	
	/**
	 * Constructor
	 * @param classtype Class type.
	 * @param cmd Command to run.
	 */
	public AddCmd(Class<?> classtype, ADataPacketAlgoCmd<ADataPacket, ?, ?> cmd){
		this.classtype = classtype;
		this.cmd = cmd;
	}
	
	/* (non-Javadoc)
	 * @see common.IAddCmd#getID()
	 */
	@Override
	public Class<?> getID() {
		return classtype;
	}


	/* (non-Javadoc)
	 * @see common.IAddCmd#getCmd()
	 */
	@Override
	public ADataPacketAlgoCmd<ADataPacket, ?, ?> getCmd() {
		return cmd;
	}

	
}

package message;

import common.IFailMsg;

/**
 * 
 * Implementation of IFailMsg
 * @author qt4
 *
 *
 */
public class FailMsg implements IFailMsg {

	/**
	 * Serial number
	 */
	private static final long serialVersionUID = -4352789220345688353L;
	/**
	 * Message text
	 */
	private String msg;

	/**
	 * Constructor
	 * @param msg Message text
	 */
	public FailMsg(String msg){
		this.msg = msg;
	}
	/* (non-Javadoc)
	 * @see common.IFailMsg#getErrorText()
	 */
	@Override
	public String getErrorText() {
		return msg;
	}

}

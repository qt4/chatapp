package chat;

import java.rmi.RemoteException;
import java.util.UUID;
import java.util.Vector;

import common.IChatroom;
import common.IPerson;

/**
 * Implementation of IPerson.
 * @author qt4
 *
 */
public class Person implements IPerson{
	/**
	 * List of person stubs.
	 */
	public Vector<IPerson> personList = new Vector<IPerson>();
	/**
	 * person name
	 */
	private String personName = System.getProperty("user.name");
	/**
	 * uuid
	 */
	private final UUID uuid = UUID.randomUUID();

	/* (non-Javadoc)
	 * @see common.IPerson#connectBack(common.IPerson)
	 */
	@Override
	public int connectBack(IPerson myPersonStub) throws RemoteException {
//		if (personList.add(myPersonStub)) return STATUS_SUCC;
		return STATUS_FAILED;
	}

	/* (non-Javadoc)
	 * @see common.IPerson#acceptInvitation(common.IChatroom)
	 */
	@Override
	public int acceptInvitation(IChatroom chatroom) throws RemoteException {
	
		return 0;
	}

	/* (non-Javadoc)
	 * @see common.IPerson#recvRequest(common.IPerson)
	 */
	@Override
	public int recvRequest(IPerson requesterStub) throws RemoteException {
		return 0;
	}

	/* (non-Javadoc)
	 * @see common.IPerson#getName()
	 */
	@Override
	public String getName() throws RemoteException {
		return personName;
	}

	/* (non-Javadoc)
	 * @see common.IPerson#getUUID()
	 */
	@Override
	public UUID getUUID() throws RemoteException {
		return uuid;
	}

}

package chat;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.UUID;

import provided.datapacket.ADataPacket;
import common.IChatroom;
import common.IMember;

/**
 * A class of Chatroom
 * @author qt4
 *
 */
public class Chatroom implements IChatroom {
	/**
	 * Serial number
	 */
	private static final long serialVersionUID = 8512394436706430668L;
	
	/**
	 * Chatroom name
	 */
	private String chatroomName;
	/**
	 * IMember list.
	 */
	private Set<IMember> membList = new HashSet<IMember>();
	/**
	 * UUID
	 */
	private final UUID uuid = UUID.randomUUID();
	
	/**
	 * Constructor
	 * @param name Chatroom name.
	 */
	public Chatroom(String name){
		this.chatroomName=name;
	}
	
	
	/* (non-Javadoc)
	 * @see common.IChatroom#getMembers()
	 */
	@Override
	public Collection<IMember> getMembers() {
		return membList;
	}

	/* (non-Javadoc)
	 * @see common.IChatroom#addMembers(java.util.Collection)
	 */
	@Override
	public void addMembers(Collection<IMember> userstub) {
		
	}

	/* (non-Javadoc)
	 * @see common.IChatroom#addMember(common.IMember)
	 */
	@Override
	public void addMember(IMember member) {
		membList.add(member);
	}

	/* (non-Javadoc)
	 * @see common.IChatroom#removeMember(common.IMember)
	 */
	@Override
	public void removeMember(IMember userstub) {
		membList.remove(userstub);
	}


	/* (non-Javadoc)
	 * @see common.IChatroom#getName()
	 */
	@Override
	public String getName() {
		return chatroomName;
	}

	/* (non-Javadoc)
	 * @see common.IChatroom#getUUID()
	 */
	@Override
	public UUID getUUID() {
		return uuid;
	}

	/* (non-Javadoc)
	 * @see common.IChatroom#broadcastMsg(provided.datapacket.ADataPacket)
	 */
	@Override
	public Collection<ADataPacket> broadcastMsg(ADataPacket msg) {
		Collection<ADataPacket> dataPacketList = new HashSet<ADataPacket>();
		Set<IMember> memberList = new HashSet<IMember>(membList);
		for(IMember member : memberList ){
			try {
				dataPacketList.add(member.recvMessage(msg));
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
		return dataPacketList;
	}
	

}

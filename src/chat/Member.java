package chat;

import java.rmi.RemoteException;
import java.util.UUID;

import provided.datapacket.ADataPacket;
import provided.datapacket.ADataPacketAlgoCmd;
import provided.datapacket.DataPacket;
import provided.datapacket.DataPacketAlgo;
import common.ICmd2ModelAdapter;
import common.IMember;

/**
 * 
 * Implementation of IMemeber.
 * @author qt4
 *
 */
public class Member implements IMember{
	
	/**
	 * Member name
	 */
	private String userName = System.getProperty("user.name");
	/**
	 * Algorithm
	 */
	private transient DataPacketAlgo<ADataPacket, Void> algo;
	/**
	 * uuid
	 */
	private final UUID uuid = UUID.randomUUID();
	
	/**
	 * Constructor
	 */
	public Member() {
		//defaultcmd
		this.algo = new DataPacketAlgo<ADataPacket, Void>(new ADataPacketAlgoCmd<ADataPacket, Object, Void>(){
			/**
			 * 
			 */
			private static final long serialVersionUID = 5444059197884196293L;

			@Override
			public ADataPacket apply(Class<?> index, DataPacket<Object> host,
					Void... params) {
				return null;
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				// TODO Auto-generated method stub
				
			}
			
		});
		
	}
	
	/**
	 * Set the algorithm.
	 * @param algo The algorithm.
	 */
	public void setAlgo(DataPacketAlgo<ADataPacket, Void> algo) {
		this.algo = algo;
	}
	
	/**
	 * Get the algorithm.
	 * @return The algorithm.
	 */
	public DataPacketAlgo<ADataPacket, Void> getAlgo() {
		return algo;
	}
	
	
	/* (non-Javadoc)
	 * @see common.IMember#recvMessage(provided.datapacket.ADataPacket)
	 */
	@Override
	public ADataPacket recvMessage(ADataPacket msg) throws RemoteException {
		return msg.execute(algo, new Void[0]);
	}

	/* (non-Javadoc)
	 * @see common.IMember#getUUID()
	 */
	@Override
	public UUID getUUID() throws RemoteException {
		return uuid;
	}

	/* (non-Javadoc)
	 * @see common.IMember#getName()
	 */
	@Override
	public String getName() throws RemoteException {
		return userName;
	}

}

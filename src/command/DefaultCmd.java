package command;

import java.rmi.RemoteException;

import message.RequestCmd;
import message.SuccMsg;
import provided.datapacket.ADataPacket;
import provided.datapacket.ADataPacketAlgoCmd;
import provided.datapacket.DataPacket;
import common.ICmd2ModelAdapter;
import common.IRequestCmd;
import common.ISuccMsg;

/**
 * Default command
 * @author qt4
 *
 */
public class DefaultCmd extends ADataPacketAlgoCmd<ADataPacket, Object, Void> {
	/**
	 * Serial number
	 */
	private static final long serialVersionUID = -5801662411215791761L;
	/**
	 * comand to model adapter
	 */
	private transient ICmd2ModelAdapter cmd2ModelAdpt;
	
	/* (non-Javadoc)
	 * @see provided.datapacket.ADataPacketAlgoCmd#apply(java.lang.Class, provided.datapacket.DataPacket, java.lang.Object[])
	 */
	@Override
	public ADataPacket apply(Class<?> index, DataPacket<Object> host,
			Void... params) {
		try {
			host.getSender().recvMessage(new DataPacket<IRequestCmd>(IRequestCmd.class, cmd2ModelAdpt.getLocalMember(), (IRequestCmd)new RequestCmd(index)));
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return new DataPacket<ISuccMsg>(ISuccMsg.class, cmd2ModelAdpt.getLocalMember(), new SuccMsg());
	}

	/* (non-Javadoc)
	 * @see provided.datapacket.ADataPacketAlgoCmd#setCmd2ModelAdpt(common.ICmd2ModelAdapter)
	 */
	@Override
	public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
		// TODO Auto-generated method stub
		
	}
	

}

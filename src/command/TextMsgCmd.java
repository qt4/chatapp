package command;

import java.rmi.RemoteException;
import java.util.Date;

import provided.datapacket.ADataPacket;
import provided.datapacket.ADataPacketAlgoCmd;
import provided.datapacket.DataPacket;
import common.ICmd2ModelAdapter;
import common.ISuccMsg;
import common.ITextMsg;

/**
 * A TextMsg command.
 * @author qt4
 *
 */
public class TextMsgCmd extends ADataPacketAlgoCmd<ADataPacket, ITextMsg, Void>{

	/**
	 * Serial number 
	 */
	private static final long serialVersionUID = 7739938886312261765L;
	/**
	 * Command to model adapter
	 */
	private transient ICmd2ModelAdapter cmd2ModelAdpt;

	/* (non-Javadoc)
	 * @see provided.datapacket.ADataPacketAlgoCmd#apply(java.lang.Class, provided.datapacket.DataPacket, java.lang.Object[])
	 */
	@Override
	public ADataPacket apply(Class<?> index, DataPacket<ITextMsg> host, Void... params) {

		String text = host.getData().getText();
		String senderName = " ";
		try {
			senderName = host.getSender().getName();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		cmd2ModelAdpt.append(new Date().toString()+"\n"+senderName+" says: "+text);
		return new DataPacket<ISuccMsg>(ISuccMsg.class, cmd2ModelAdpt.getLocalMember(), new ISuccMsg(){

			/**
			 * 
			 */
			private static final long serialVersionUID = -1665438886700494106L;

		});
	}

	/* (non-Javadoc)
	 * @see provided.datapacket.ADataPacketAlgoCmd#setCmd2ModelAdpt(common.ICmd2ModelAdapter)
	 */
	@Override
	public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
		this.cmd2ModelAdpt = cmd2ModelAdpt;
	}

}

package controller;

import java.awt.Component;
import java.awt.EventQueue;
import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Vector;

import provided.datapacket.ADataPacket;
import common.IChatroom;
import common.IPerson;
import model.ChatModel;
import model.IMiniViewAdapter;
import model.IViewAdapter;
import model.MiniModel;
import view.ChatGUI;
import view.IMiniModelAdapter;
import view.IModelAdapter;
import view.MiniGUI;

public class ChatController {
	/**
	 * The view of the MVC structure.
	 */
	private ChatGUI<IPerson, IChatroom> chatGUI;
	/**
	 * The model of the MVC structure.
	 */
	private ChatModel chatModel;
	
	/**
	 * The constructor of the controller.
	 */
	public ChatController() {
		chatModel = new ChatModel (new IViewAdapter(){

			@Override
			public void createChatroom(String chatroomName) {
//				chatGUI.createChatroom(chatroomName);
				
			}
			
			/**
			 * make mini view adapter
			 */
			@Override
			public IMiniViewAdapter makeMiniViewAdapter(MiniModel miniModel) {
				IMiniModelAdapter miniModelAdapter = new IMiniModelAdapter() {
					@Override
					public void sendMsg(String text) {
						try {
							miniModel.sendMsg(text);
						} catch (RemoteException e) {
							e.printStackTrace();
						}
						
					}

					@Override
					public Collection<ADataPacket> leave() {
						return miniModel.leave();
						
					}

					@Override
					public String getMemberList() {
						return miniModel.getMemberList();
					}

						
				};				
				
				/**
				 * make mini MVC
				 */
				MiniGUI miniGUI = chatGUI.makeMiniGUI(miniModelAdapter);
				miniGUI.start();

				IMiniViewAdapter miniViewAdapter = new IMiniViewAdapter() {

					@Override
					public void append(String s) {
						miniGUI.append(s);
						
					}

					@Override
					public void updateList() {
						miniGUI.updateList();
					}

					@Override
					public void removeChatroom(IChatroom chatroom) {
						chatModel.removeChatroom(chatroom);
						
					}

					@Override
					public void setTitle(String name) {
						miniGUI.setTitle(name);
						
					}

					@Override
					public void removeModel(MiniModel miniModel) {
						chatModel.removeModel(miniModel);
						
					}

					@Override
					public void addComponent(String name, Component newComp) {
						miniGUI.addComponent(name, newComp);
						
					}

				};	
				
				return miniViewAdapter;
			}

			@Override
			public void updateList() {
				chatGUI.updateList();
				
			}

		});
		
		/**
		 * Instantiate main view
		 */
		chatGUI = new ChatGUI <IPerson, IChatroom>(new IModelAdapter<IPerson, IChatroom>(){

			@Override
			public void connectTo(String hostIP) {
				chatModel.connectTo(hostIP);
			}

			@Override
			public void quit() {
				chatModel.stop();
				
			}

			@Override
			public void createChatroom(String name) {
				chatModel.createChatroom(name);
				
			}

			@Override
			public void invite(IPerson selectedPerson, IChatroom selectedChatroom) {
				chatModel.invite(selectedPerson, selectedChatroom);
				
			}

			@Override
			public Vector<IPerson> getPersons() {
				return chatModel.getPersons();
			}

			@Override
			public Vector<IChatroom> getChatrooms() {
				return chatModel.getChatrooms();
			}

			@Override
			public void request(IPerson selectedPerson) {
				chatModel.request(selectedPerson);
			}

			@Override
			public void leave() {
				chatModel.leave();;
			}
			
		});

	}

	/**
	 * Start the system.
	 */
	public void start() {
		chatModel.start();
		chatGUI.start();
	}


	/**
	 * Launch the application.
	 * @param args Arguments given by the system or command line.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ChatController controller = new ChatController();
					controller.start();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	}

}

package view;

import java.util.Vector;
/**
 * Main view to model adapter
 * @author qt4
 *
 * @param <TListItem> Person list
 * @param <TRoomItem> Chatroom list
 */
public interface IModelAdapter<TListItem, TRoomItem> {

	/**
	 * Connect to a person
	 * @param hostIP IP
	 */
	public void connectTo(String hostIP);

	/**
	 * Quit the application
	 */
	public void quit();

	/**
	 * Create a chatroom
	 * @param name Chatroom name
	 */
	public void createChatroom(String name);

	/**
	 * Invite a person to join a chatroom
	 * @param selectedPerson Person
	 * @param selectedRoom Chatroom
	 */
	public void invite(TListItem selectedPerson, TRoomItem selectedRoom);

	/**
	 * Get person list
	 * @return person list
	 */
	public Vector<TListItem> getPersons();

	/**
	 * Get chatroom list
	 * @return chatroom list
	 */
	public Vector<TRoomItem> getChatrooms();

	/**
	 * Request to join other's chatroom
	 * @param selectedPerson person
	 */
	public void request(TListItem selectedPerson);

	/**
	 * Leave a chatroom
	 */
	public void leave();

}

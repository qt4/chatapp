package view;

import java.awt.BorderLayout;
import java.awt.Component;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JSplitPane;
import javax.swing.JTextArea;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.GridLayout;

import javax.swing.border.TitledBorder;
import javax.swing.border.BevelBorder;
import javax.swing.JScrollPane;

/**
 * Mini view
 * @author qt4
 *
 */
public class MiniGUI extends JFrame {

	/**
	 * Serial number
	 */
	private static final long serialVersionUID = 5884960551930328760L;
	
	private JPanel contentPane;
	private final JPanel panel = new JPanel();
	private final JSplitPane splitPane = new JSplitPane();
	private final JPanel panel_1 = new JPanel();
	private final JTextArea textSend = new JTextArea();
	private final JButton btnSend = new JButton("Send");
	private final JTextArea txtrDisplay = new JTextArea();
	private final JPanel pnlUser = new JPanel();

	private IMiniModelAdapter miniModelAdapter;
	private final JTextArea txtrMemberlist = new JTextArea();
	private final JPanel panelShow = new JPanel();

	/**
	 * Create the frame.
	 * @param miniModelAdapter mini view to model adapter
	 */
	public MiniGUI(IMiniModelAdapter miniModelAdapter) {
		this.miniModelAdapter=miniModelAdapter;
		initGUI();
	}
	/**
	 * Initialize the view
	 */
	private void initGUI() {
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		addWindowListener(new java.awt.event.WindowAdapter() {
			public void windowClosing(java.awt.event.WindowEvent e) {
				leaveChatroom();
				MiniGUI.this.dispose();
			}
		});
		setBounds(100, 100, 572, 425);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		contentPane.add(panel, BorderLayout.NORTH);
		splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);

		contentPane.add(splitPane, BorderLayout.CENTER);

		splitPane.setRightComponent(panel_1);
		textSend.setLineWrap(true);
		textSend.setBorder(new BevelBorder(BevelBorder.LOWERED, null, null, null, null));
		textSend.setRows(3);
		textSend.setColumns(30);

		panel_1.add(textSend);
		btnSend.setToolTipText("Send text");
		btnSend.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				miniModelAdapter.sendMsg(textSend.getText());
				textSend.setText(null);
				//				updateList();
			}
		});

		panel_1.add(btnSend);
		txtrDisplay.setLineWrap(true);
		txtrDisplay.setEditable(false);

		JScrollPane scroll = new JScrollPane();
		scroll.setAutoscrolls(true);
		scroll.setViewportView(txtrDisplay);
		//		contentPane.add(scroll, BorderLayout.CENTER);


		splitPane.setLeftComponent(scroll);
		//		splitPane.setLeftComponent(txtrDisplay);
		splitPane.setDividerLocation(260);
		pnlUser.setBorder(new TitledBorder(null, "Member List", TitledBorder.LEADING, TitledBorder.TOP, null, null));

		contentPane.add(pnlUser, BorderLayout.EAST);
		pnlUser.setLayout(new GridLayout(0, 1, 0, 0));
		txtrMemberlist.setColumns(10);
		txtrMemberlist.setEditable(false);

		pnlUser.add(txtrMemberlist);
		
		contentPane.add(panelShow, BorderLayout.WEST);
	}

	/**
	 * Start the view
	 */
	public void start(){
		setVisible(true);
	}


	/**
	 * Display received text.
	 * @param s text received.
	 */
	public void append(String s) {
		txtrDisplay.append(s+"\n\n");
		txtrDisplay.setCaretPosition(txtrDisplay.getDocument().getLength());
	}

	/**
	 * Update the member list
	 */
	public void updateList(){
		txtrMemberlist.setText(miniModelAdapter.getMemberList());
	}

	/**
	 * Leave a chatroom
	 */
	public void leaveChatroom() {
		miniModelAdapter.leave();
	}
	
	/**
	 * Add a component
	 * @param name Name
	 * @param newComp Component to add
	 */
	public void addComponent(String name, Component newComp) {
		panelShow.add(newComp, name);	
	}
	
}

package view;

import java.util.Collection;

import provided.datapacket.ADataPacket;

/**
 * Mini view to model adapter
 * @author qt4
 *
 */
public interface IMiniModelAdapter {

	/**
	 * Send message
	 * @param text Message text
	 */
	void sendMsg(String text);

	/**
	 * Leave a chatroom
	 * @return Return message.
	 */
	Collection<ADataPacket> leave();

	/**
	 * Get member list
	 * @return member list
	 */
	String getMemberList();
	
}

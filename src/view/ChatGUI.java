package view;

import java.awt.BorderLayout;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.WindowConstants;

import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;

import java.awt.Component;

import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.rmi.RemoteException;

import javax.swing.JList;

import common.IChatroom;
import common.IPerson;

import javax.swing.border.TitledBorder;
import java.awt.GridLayout;

/**
 * Main view of the application
 * @author qt4
 *
 * @param <TListItem> Person list
 * @param <TRoomItem> Chatroom list
 */
public class ChatGUI<TListItem, TRoomItem> extends JFrame {

	/**
	 * serial number
	 */
	private static final long serialVersionUID = 4818357684393494077L;
	private JPanel contentPane;
	private final JPanel pnlControl = new JPanel();
	private final JLabel lblIp = new JLabel("IP");
	private final JTextField tfIP = new JTextField();
	private final JButton btnConnect = new JButton("Connect");
	private final JPanel pnlSend = new JPanel();
	private final JSplitPane splitPane = new JSplitPane();
	private final JTabbedPane tabbedPane = new JTabbedPane(JTabbedPane.TOP);
	private final JButton btnRequest = new JButton("Request");
	private final JButton btnInvite = new JButton("Invite");

	private IModelAdapter<TListItem, TRoomItem> modelAdapter;
	private final JButton btnNew = new JButton("New Chatroom");

	/**
	 * Person list
	 */
	private JList<TListItem> listPerson = new JList<TListItem>();

	private transient ListCellRenderer personRenderer = new ListCellRenderer();
	private ChatListCellRenderer chatRenderer = new ChatListCellRenderer();
	private final JPanel panel = new JPanel();
	private final JList<TRoomItem> listChatroom = new JList<TRoomItem>();

	/**
	 * Create the frame.
	 * @param modelAdapter view to model adapter
	 */
	public ChatGUI(IModelAdapter<TListItem, TRoomItem> modelAdapter) {
		this.modelAdapter=modelAdapter;
		tfIP.setText("localhost");
		tfIP.setColumns(10);
		initGUI();

	}

	/**
	 * Start the view.
	 */
	public void start() {
		setVisible(true);
		listPerson.setCellRenderer(personRenderer);
		listPerson.setListData(modelAdapter.getPersons());
		listChatroom.setCellRenderer(chatRenderer);
		listChatroom.setListData(modelAdapter.getChatrooms());
	}

	/**
	 * Init the GUI.
	 */
	private void initGUI() {
		setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
		this.addWindowListener(new WindowAdapter() {
			public void windowClosing(WindowEvent e) {
				leaveChatrooms();
				quit();
			}
		});

		setBounds(100, 100, 610, 298);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);

		contentPane.add(pnlControl, BorderLayout.NORTH);

		pnlControl.add(lblIp);

		pnlControl.add(tfIP);
		btnConnect.setToolTipText("Connect to a host");
		btnConnect.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//				listPerson = new JList<IPerson>(modelAdapter.getPersons());
				modelAdapter.connectTo(tfIP.getText());
				//				listPerson.setListData(modelAdapter.getPersons());
				//				
			}
		});

		pnlControl.add(btnConnect);
		btnNew.setToolTipText("Create a new Chatroom");
		btnNew.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//				JPanel jp = new JPanel();
				//				jp.add(new JTextArea());
				//				tabbedPane.add("Chat Room "+(idx++), new JTextArea());
				String name = JOptionPane.showInputDialog(null, "Please input a name for the Chatroom!");
				if (name!=null) {
					if (name.equals("")) name = "Chatroom Name";
					modelAdapter.createChatroom(name);
				}

			}
		});


		pnlControl.add(btnNew);
		btnRequest.setToolTipText("Request to join a Chatroom of some person's.");
		btnRequest.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!listPerson.isSelectionEmpty()) {
					// Instantiate a thread object
					Thread requestThread = new Thread( new Runnable() {
						public void run() {
							modelAdapter.request(listPerson.getSelectedValue());
						}
					});

					// Start the new thread running.   
					requestThread.start();    // This runs the code in the run() method above.   
				} else {
					JOptionPane.showMessageDialog(null, "Please select a person to request!", "Error ", JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		pnlControl.add(btnRequest);
		btnInvite.setToolTipText("Invite a person to join a Chatroom in the list");
		btnInvite.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (!listPerson.isSelectionEmpty() && !listChatroom.isSelectionEmpty()){
					modelAdapter.invite(listPerson.getSelectedValue(), listChatroom.getSelectedValue());
				} else {
					JOptionPane.showMessageDialog(null, "Please select Person and Chatroom!", "Error ", JOptionPane.ERROR_MESSAGE);
				}
			}
		});

		pnlControl.add(btnInvite);

		contentPane.add(pnlSend, BorderLayout.SOUTH);
		splitPane.setDividerSize(0);
		splitPane.setEnabled(false);

		contentPane.add(splitPane, BorderLayout.CENTER);

		splitPane.setLeftComponent(tabbedPane);

		splitPane.setRightComponent(panel);
		panel.setLayout(new GridLayout(0, 2, 0, 0));
		listPerson.setBorder(new TitledBorder(null, "Person List", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.add(listPerson);
		listChatroom.setBorder(new TitledBorder(null, "Chatroom List", TitledBorder.LEADING, TitledBorder.TOP, null, null));

		panel.add(listChatroom);
	}


	/**
	 * Centralized management point for exiting the application.
	 * All calls to exit the system should go through here.
	 * Shuts system down by stopping the model.
	 */
	private void quit() {
		System.out.println("ChatGUI: Server is quitting...");
		modelAdapter.quit();
	}



	/**
	 * Make mini view
	 * @param miniModelAdapter mini view to model adapter
	 * @return Mini view
	 */
	public MiniGUI makeMiniGUI (IMiniModelAdapter miniModelAdapter) {
		MiniGUI miniGUI = new MiniGUI(miniModelAdapter);
		return miniGUI;
	}

	/**
	 * Refresh the list of person and chatroom
	 */
	public void updateList() {
		listPerson.updateUI();
		listChatroom.updateUI();
	}



	/**
	 * List renderer for person to show person name.
	 *
	 */
	private class ListCellRenderer extends DefaultListCellRenderer {

		/**
		 * 
		 */
		private static final long serialVersionUID = 2011911360196450913L;

		@Override
		public Component getListCellRendererComponent(JList<?> list, Object value,
				int index, boolean isSelected, boolean cellHasFocus) {
			if (value != null && value instanceof IPerson) {
				try {
					value  = ((IPerson)value).getName();
				} catch (RemoteException e) {
					e.printStackTrace();
				}
			}
			return super.getListCellRendererComponent(list, value, index,
					isSelected, cellHasFocus);
		}

	}

	/**
	 * List renderer for chatroom to show chatroom name.
	 */
	private class ChatListCellRenderer extends DefaultListCellRenderer {
		/**
		 * 
		 */
		private static final long serialVersionUID = 8997753793677953106L;

		@Override
		public Component getListCellRendererComponent(JList<?> list, Object value,
				int index, boolean isSelected, boolean cellHasFocus) {
			if (value != null && value instanceof IChatroom) {
				value  = ((IChatroom)value).getName();
			}
			return super.getListCellRendererComponent(list, value, index,
					isSelected, cellHasFocus);
		}

	}


	/**
	 * Leave a chatroom
	 */
	public void leaveChatrooms() {
		modelAdapter.leave();
	}
}

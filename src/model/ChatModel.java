package model;

import java.rmi.RemoteException;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.Vector;

import javax.swing.JOptionPane;

import message.JoinRoomMsg;
import message.TextMsg;
import chat.Chatroom;
import chat.Member;
import chat.Person;
import common.IChatroom;
import common.IJoinRoomMsg;
import common.IMember;
import common.IPerson;
import common.ITextMsg;
import provided.datapacket.DataPacket;
import provided.rmiUtils.IRMIUtils;
import provided.rmiUtils.IRMI_Defs;
import provided.rmiUtils.RMIUtils;
import provided.util.IVoidLambda;


/**
 * Main model of the application.
 * @author qt4
 *
 */
/**
 * @author qt4
 *
 */

public class ChatModel {
	/**
	 * Model to view adapter
	 */
	private IViewAdapter viewAdapter;

	/**
	 * A list to store mini models
	 */
	private Vector<MiniModel> modelList = new Vector<MiniModel>();

	/**
	 * A list to store chatrooms.
	 */
	private Vector<IChatroom> chatroomList = new Vector<IChatroom>();

	/**
	 * Person object.
	 */
	private Person self;

	/**
	 * Person stub
	 */
	private IPerson selfStub;

	/**
	 * The RMI Registry
	 */
	private Registry registry;


	/**
	 * Constructor
	 * @param viewAdapter Adapter
	 */
	public ChatModel(IViewAdapter viewAdapter){
		this.viewAdapter=viewAdapter;
	}


	/**
	 * A command used as a wrapper around the view adapter for the IRMIUtils and the ComputeEngine.
	 */
	private IVoidLambda<String> outputCmd = new IVoidLambda<String> (){
		public void apply(String... strs){
		}
	};

	/**
	 * Utility object used to get the Registry
	 */
	private IRMIUtils rmiUtils = new RMIUtils(outputCmd);


	/**
	 * Start the server by setting the necessary RMI system parameters, starting the security manager, 
	 * locating the local Registry and binding an instance of the ComputeEngine to it.  
	 * Also starts the class file server to enable remote dynamic class loading.
	 */
	public void start() {
		rmiUtils.startRMI(IRMI_Defs.CLASS_SERVER_PORT_SERVER);

		try {
			self = new Person(){

				@Override
				public int connectBack(IPerson myPersonStub) throws RemoteException {
					if (personList.add(myPersonStub)) {
						viewAdapter.updateList();
						return IPerson.STATUS_SUCC;
					}
					return IPerson.STATUS_FAILED;
				}

				@Override
				public int acceptInvitation(IChatroom chatroom) throws RemoteException {
					Member selfuser = new Member();
					System.out.println("Instantiated new IMember");
					IMember selfuserStub = null;
					try {
						selfuserStub = (IMember) UnicastRemoteObject.exportObject(selfuser, 2012);
						chatroom.addMember(selfuserStub);
					} catch (RemoteException e) {
						e.printStackTrace();
					}

					MiniModel miniModel = new MiniModel();
					miniModel.setAdapter(viewAdapter.makeMiniViewAdapter(miniModel));
					miniModel.setChatroom(chatroom);
					miniModel.setSelf(selfuser);
//					miniModel.setSelfStub(selfuserStub);
					modelList.add(miniModel);
					chatroomList.add(chatroom);
					//					viewAdapter.updateList();

					System.out.println("member size: "+chatroom.getMembers().size());

					chatroom.broadcastMsg(new DataPacket<IJoinRoomMsg>(IJoinRoomMsg.class, selfuserStub, new JoinRoomMsg()));
					chatroom.broadcastMsg(new DataPacket<ITextMsg>(ITextMsg.class, selfuserStub, new TextMsg(selfuserStub.getName()+" joined in the Chatroom.")));
					System.out.println("member size: "+chatroom.getMembers().size());
					viewAdapter.updateList();

					return IPerson.STATUS_SUCC;
				}


				@Override
				public int recvRequest(IPerson requesterStub) throws RemoteException {
					if (!chatroomList.isEmpty()) {
						IChatroom selChatroom = (IChatroom) JOptionPane.showInputDialog(null,
								"Received a request from: "+ requesterStub.getName()
								+ ".\nPlease select a Chatroom: ",
								"Request a Chatroom",
								JOptionPane.PLAIN_MESSAGE,
								null,  chatroomList.toArray(), null
								);
						requesterStub.acceptInvitation(selChatroom);
						return IPerson.STATUS_SUCC;
					} else {
						int n = JOptionPane.showConfirmDialog(
								null,
								"Received a request from: "+ requesterStub.getName()
								+ ".\nWould you accept it? ",
								"Chat Request",
								JOptionPane.YES_NO_OPTION);
						if (n==0) {
							Chatroom chatroom = createChatroom("Auto Created");
							requesterStub.acceptInvitation(chatroom);
							return IPerson.STATUS_SUCC;
						}
					}
					return IPerson.STATUS_FAILED;
				}

			};
			System.out.println("Instantiated new IPerson");
			selfStub =(IPerson) UnicastRemoteObject.exportObject(self, IPerson.BOUND_PORT);
			registry = rmiUtils.getLocalRegistry();
			System.out.println("Found registry: "+ registry);
			//			view.append("Found registry: "+ registry+ "\n");
			registry.rebind(IPerson.BOUND_NAME, selfStub);
			System.out.println("IPerson bound to "+IPerson.BOUND_NAME);
			//			view.append("ComputeEngine bound to "+ICompute.BOUND_NAME+"\n");
		} 
		catch (Exception e) {
			System.err.println("ChatApp exception:"+"\n");
			e.printStackTrace();
			System.exit(-1);
		}
	}


	/**
	 * Stops the EngineModel by unbinding the ComputeEngine from the Registry 
	 * and stopping class file server. 
	 */
	public void stop() {
		try {
			registry.unbind(IPerson.BOUND_NAME);
			System.out.println("EngineController: " + IPerson.BOUND_NAME
					+ " has been unbound.");

			rmiUtils.stopRMI();

			System.exit(0);
		} catch (Exception e) {
			System.err.println("EngineController: Error unbinding "
					+ IPerson.BOUND_NAME + ":\n" + e);
			System.exit(-1);
		}
	}



	/**
	 * Connects to the given remote host and retrieves the stub to the ICompute object bound 
	 * to the ICompute.BOUND_NAME name in the remote Registry on port 
	 * IRMI_Defs.REGISTRY_PORT.  
	 * 
	 * @param remoteHost The IP address or host name of the remote server.
	 * @return  A status string on the connection.
	 */
	public String connectTo(String remoteHost) {
		try {

			Registry registry = rmiUtils.getRemoteRegistry(remoteHost);
			IPerson person = (IPerson) registry.lookup(IPerson.BOUND_NAME);

			if ((self.personList.add(person))&&(person.connectBack(selfStub)==IPerson.STATUS_SUCC))
				viewAdapter.updateList();

			System.out.println("personlist size: "+self.personList.size());

		} catch (Exception e) {
			e.printStackTrace();
			return "No connection established!";
		}
		return "Connection to " + remoteHost + " established!";
	}


	/**
	 * Create a chatroom
	 * @param name chatroom name
	 * @return A chatroom
	 */
	public Chatroom createChatroom(String name) {
		Chatroom chat = new Chatroom(name);
		chatroomList.add(chat);
		viewAdapter.updateList();

		System.out.println("chatroomList size: "+chatroomList.size());
		Member selfuser = new Member();
		System.out.println("Instantiated new IMember");
		IMember selfuserStub = null;
		try {
			selfuserStub = (IMember) UnicastRemoteObject.exportObject(selfuser, 2012);
			chat.addMember(selfuserStub);
		} catch (RemoteException e) {
			e.printStackTrace();
		}

		MiniModel miniModel = new MiniModel();
		miniModel.setAdapter(viewAdapter.makeMiniViewAdapter(miniModel));
		miniModel.setChatroom(chat);
		miniModel.setSelf(selfuser);
		modelList.add(miniModel);
		return chat;

	}


	/**
	 * Invite a person to join a chatroom
	 * @param selectedPerson Person to join.
	 * @param selectedChatroom Chatroom to join.
	 */
	public void invite(IPerson selectedPerson, IChatroom selectedChatroom) {
		try {
			selectedPerson.acceptInvitation(selectedChatroom);

		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}


	/**
	 * Get the person list.
	 * @return Person list.
	 */
	public Vector<IPerson> getPersons() {
		return self.personList;
	}


	/**
	 * Get the chatroom list.
	 * @return Chatroom list
	 */
	public Vector<IChatroom> getChatrooms() {
		return chatroomList;
	}


	/**
	 * Requeste to join a chatroom.
	 * @param requestee The person being requested.
	 */
	public void request(IPerson requestee) {
		try {
			int res = requestee.recvRequest(selfStub);
			if (res == IPerson.STATUS_FAILED) {
				JOptionPane.showMessageDialog(null, "The request is rejected");
			}
		} catch (RemoteException e) {
			e.printStackTrace();
		}

	}


	/**
	 * Remove a chatroom
	 * @param chatroom Chatroom to remove
	 */
	public void removeChatroom(IChatroom chatroom) {
		chatroomList.remove(chatroom);
		viewAdapter.updateList();	
	}

	/**
	 * Remove a model
	 * @param miniModel model to remove.
	 */
	public void removeModel (MiniModel miniModel) {
		modelList.remove(miniModel);
	}

	/**
	 * Leave a chatroom
	 */
	public void leave() {
		Vector<MiniModel> mList = new Vector<MiniModel>(modelList);
		for(MiniModel mm : mList ){
			mm.leave();
		}

	}

}

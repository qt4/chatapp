package model;

import java.rmi.RemoteException;
import java.util.Date;

import message.AddCmd;
import message.SuccMsg;
import common.ICmd2ModelAdapter;
import common.IFailMsg;
import common.IRequestCmd;
import common.ISuccMsg;
import common.ITextMsg;
import common.IAddCmd;
import provided.datapacket.ADataPacket;
import provided.datapacket.ADataPacketAlgoCmd;
import provided.datapacket.DataPacket;
import provided.datapacket.DataPacketAlgo;
import provided.extvisitor.IExtVisitorCmd;

/**
 * Message algorithm
 * @author qt4
 *
 */
public class MsgAlgo extends DataPacketAlgo<ADataPacket, Void> {

	/**
	 * serial number
	 */
	private static final long serialVersionUID = 762868564162394668L;
	/**
	 * Command to model adapter
	 */
	private transient ICmd2ModelAdapter cmd2ModelAdpt;


	/**
	 * Constructor
	 * @param defaultCmd Default command
	 */
	public MsgAlgo(ADataPacketAlgoCmd<ADataPacket, Object, Void> defaultCmd) {
		super(defaultCmd);


		/**
		 *  set TextMsg Command.
		 */
		this.setCmd(ITextMsg.class, new ADataPacketAlgoCmd<ADataPacket, ITextMsg, Void>(){

			/**
			 * 
			 */
			private static final long serialVersionUID = 4488472418196793722L;

			@Override
			public ADataPacket apply(Class<?> index, DataPacket<ITextMsg> host,
					Void... params) {
				String text = host.getData().getText();
				String senderName = " ";
				try {
					senderName = host.getSender().getName();
				} catch (RemoteException e) {
					e.printStackTrace();
				}
				MsgAlgo.this.cmd2ModelAdpt.append(new Date().toString()+"\n"+senderName+" says: "+text);
				return new DataPacket<ISuccMsg>(ISuccMsg.class, cmd2ModelAdpt.getLocalMember(), new SuccMsg());
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {

			}

		});

		/**
		 *  Set RequestCmd.
		 */
		this.setCmd(IRequestCmd.class, new ADataPacketAlgoCmd<ADataPacket, IRequestCmd, Void>(){

			/**
			 * 
			 */
			private static final long serialVersionUID = 2313399919455080516L;

			@SuppressWarnings("unchecked")
			@Override
			public ADataPacket apply(Class<?> index, DataPacket<IRequestCmd> host,
					Void... params) {
				System.out.println("Running IRequestCmd.");

				Class<?> idx = host.getData().getID();
				try {
					host.getSender().recvMessage(new DataPacket<IAddCmd>(IAddCmd.class, cmd2ModelAdpt.getLocalMember(), new AddCmd(idx, (ADataPacketAlgoCmd<ADataPacket, ?, ?>) MsgAlgo.this.getCmd(idx))));
				} catch (RemoteException e) {
					e.printStackTrace();
				}
				return new DataPacket<IAddCmd>(IAddCmd.class, cmd2ModelAdpt.getLocalMember(), new AddCmd(idx, (ADataPacketAlgoCmd<ADataPacket, ?, ?>) MsgAlgo.this.getCmd(idx)));

			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
	
			}
			
		});

		this.setCmd(IAddCmd.class, new ADataPacketAlgoCmd<ADataPacket, IAddCmd, Void>(){

			/**
			 * 
			 */
			private static final long serialVersionUID = -1655939950799464594L;

			@SuppressWarnings("unchecked")
			@Override
			public ADataPacket apply(Class<?> index, DataPacket<IAddCmd> host,
					Void... params) {
				
				
				MsgAlgo.this.setCmd(host.getData().getID(), (IExtVisitorCmd<ADataPacket, Class<?>, Void, ADataPacket>) host.getData().getCmd());
				return new DataPacket<ISuccMsg>(ISuccMsg.class, cmd2ModelAdpt.getLocalMember(), new SuccMsg());
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				
			}

			
			
		});
		
		
		/**
		 *  success message
		 */
		this.setCmd(ISuccMsg.class, new ADataPacketAlgoCmd<ADataPacket, ISuccMsg, Void>(){

		
			/**
			 * 
			 */
			private static final long serialVersionUID = -448317610685305867L;

			@Override
			public ADataPacket apply(Class<?> index, DataPacket<ISuccMsg> host,
					Void... params) {
				return null;
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {

			}

		});
		
		
		/**
		 * 	fail message
		 */
		this.setCmd(IFailMsg.class, new ADataPacketAlgoCmd<ADataPacket, IFailMsg, Void>(){

			
			/**
			 * 
			 */
			private static final long serialVersionUID = -6118775019987374522L;

			@Override
			public ADataPacket apply(Class<?> index, DataPacket<IFailMsg> host,
					Void... params) {
				return null;
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {

			}

		});


	}

	public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
		this.cmd2ModelAdpt = cmd2ModelAdpt;

	}

}

package model;

import java.awt.Component;
import java.rmi.RemoteException;
import java.util.Collection;

import chat.Member;
import message.LeaveRoomMsg;
import message.SuccMsg;
import message.TextMsg;
import provided.datapacket.ADataPacket;
import provided.datapacket.ADataPacketAlgoCmd;
import provided.datapacket.DataPacket;
import provided.mixedData.IMixedDataDictionary;
import command.DefaultCmd;
import common.ICmd2ModelAdapter;
import common.IChatroom;
import common.IJoinRoomMsg;
import common.ILeaveRoomMsg;
import common.IMember;
import common.ISuccMsg;
import common.ITextMsg;

/**
 * Mini model
 * @author qt4
 *
 */
public class MiniModel {
	/**
	 * mini model to view adapter
	 */
	private transient IMiniViewAdapter miniViewAdapter;
	
	/**
	 * Member 
	 */
	private Member self;
	
	/**
	 * Member stub
	 */
//	private IMember selfStub;;
	
	/**
	 * Chatroom
	 */
	private IChatroom chatroom;
	
	/**
	 * Algorithm
	 */
	private MsgAlgo msgAlgo = new MsgAlgo(new DefaultCmd());

	
	/**
	 * Constructor
	 */
	public MiniModel(){
		msgAlgo.setCmd2ModelAdpt(cmd2ModelAdpt);
		
		msgAlgo.setCmd(IJoinRoomMsg.class, new ADataPacketAlgoCmd<ADataPacket, IJoinRoomMsg, Void>(){

			/**
			 * 
			 */
			private static final long serialVersionUID = -4141523923418651422L;

			@Override
			public ADataPacket apply(Class<?> index,
					DataPacket<IJoinRoomMsg> host, Void... params) {
				chatroom.addMember(host.getSender());
				miniViewAdapter.updateList();
				return new DataPacket<ISuccMsg>(ISuccMsg.class, cmd2ModelAdpt.getLocalMember(), new SuccMsg());
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				
			}

			

		});
		
		
		msgAlgo.setCmd(ILeaveRoomMsg.class, new ADataPacketAlgoCmd<ADataPacket, ILeaveRoomMsg, Void>(){
			/**
			 * 
			 */
			private static final long serialVersionUID = 5131718727416573467L;

			@Override
			public ADataPacket apply(Class<?> index,
					DataPacket<ILeaveRoomMsg> host, Void... params) {
				chatroom.removeMember(host.getSender());
				miniViewAdapter.updateList();
				return new DataPacket<ISuccMsg>(ISuccMsg.class, cmd2ModelAdpt.getLocalMember(), new SuccMsg());
			}

			@Override
			public void setCmd2ModelAdpt(ICmd2ModelAdapter cmd2ModelAdpt) {
				
			}

		});
		
	}
	
	
	/**
	 * Set the stub of the chatroom
	 * @param selfuser Member
	 */
	public void setSelf(Member selfuser) {
		self = selfuser; 
		self.setAlgo(msgAlgo);
	}
	

	/**
	 * Set the adapter
	 * @param miniViewAdapter Adapter
	 */
	public void setAdapter(IMiniViewAdapter miniViewAdapter) {
		this.miniViewAdapter=miniViewAdapter;
	}
	
	
	/**
	 * Set the chatroom
	 * @param chatroom chatroom
	 */
	public void setChatroom (IChatroom chatroom){
		this.chatroom=chatroom;
		miniViewAdapter.updateList();
		miniViewAdapter.setTitle("Chatroom Name: "+chatroom.getName());
	}
	

	/**
	 * Command to model adapter
	 */
	final transient ICmd2ModelAdapter cmd2ModelAdpt = new ICmd2ModelAdapter(){

		@Override
		public void append(String s) {
			MiniModel.this.append(s);
			
		}
		@Override
		public void addComponent(String name, Component newComp) {
			MiniModel.this.addComponent(name, newComp);
		}
		@Override
		public IMember getLocalMember() {
			return self;
		}
		@Override
		public IMixedDataDictionary getDataDict() {
			// TODO Auto-generated method stub
			return null;
		}
		
	};
	
	/**
	 * Send text message
	 * @param text Message to send
	 * @throws RemoteException remote exception
	 */
	public void sendMsg(String text) throws RemoteException {
		if (!text.equals("")) {
			chatroom.broadcastMsg(new DataPacket<ITextMsg>(ITextMsg.class, this.self, new TextMsg(text)));
			
//			chatroom.broadcastMsg(new DataPacket<IJoinRoomMsg>(IJoinRoomMsg.class, this.self, new JoinRoomMsg()));
			
		}
	}

	/**
	 * Display text
	 * @param s text
	 */
	public void append(String s) {
		miniViewAdapter.append(s);
		
	}


	/**
	 * Leave chatroom
	 * @return retrun message.
	 */
	public Collection<ADataPacket> leave() {
		Collection<ADataPacket> res =chatroom.broadcastMsg(new DataPacket<ILeaveRoomMsg>(ILeaveRoomMsg.class, this.self, new LeaveRoomMsg()));
		miniViewAdapter.removeChatroom(chatroom);
		miniViewAdapter.removeModel(this);
		return res;	
	}


	/**
	 * Get member list in a chatroom
	 * @return Member list
	 */
	public String getMemberList() {
		Collection<IMember> memberList = chatroom.getMembers();
		StringBuilder s = new StringBuilder();
		for(IMember member : memberList ){
			try {
				s.append(member.getName());
				s.append(System.getProperty("line.separator"));
			} catch (RemoteException e) {
				e.printStackTrace();
			}
		}
		return s.toString();
	}

	/**
	 * Get a chatroom
	 * @return chatroom
	 */
	public IChatroom getChatroom() {
		return chatroom;
	}
	
	/**
	 * Add a new component
	 * @param name Name
	 * @param newComp New component
	 */
	public void addComponent(String name, Component newComp) {
		miniViewAdapter.addComponent(name, newComp);
	}
}

package model;

import java.awt.Component;

import common.IChatroom;

/**
 * Mini model to view adapter.
 * @author qt4
 *
 */
public interface IMiniViewAdapter {

	/**
	 * Add text
	 * @param s text
	 */
	void append(String s);

	/**
	 * Update the person list.
	 */
	void updateList();

	/**
	 * Remove a chatroom
	 * @param chatroom Chatroom to remove
	 */
	void removeChatroom(IChatroom chatroom);

	/**
	 * Set the chatroom title
	 * @param name Title to set.
	 */
	void setTitle(String name);

	/**
	 * Remove a mini model
	 * @param miniModel Mini model to remove
	 */
	void removeModel(MiniModel miniModel);

	/**
	 * Add a new component
	 * @param name	 the name of the component
	 * @param newComp	the new component
	 */
	void addComponent(String name, Component newComp);
	
}

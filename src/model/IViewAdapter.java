package model;

/**
 * Main model to view adapter.
 * @author qt4
 *
 */
public interface IViewAdapter {

	/**
	 * Create a chatroom
	 * @param chatroomName Name of the chatroom
	 */
	public void createChatroom(String chatroomName);
	
	/**
	 * Maker of a mini view adapter
	 * @param mm Mini model
	 * @return A mini model to view adapter.
	 */
	public IMiniViewAdapter makeMiniViewAdapter (MiniModel mm);

	/**
	 * Update the member list.
	 */
	public void updateList();

}
